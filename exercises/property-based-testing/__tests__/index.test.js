const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);
const sortDesc = (data) => data.slice().sort((a, b) => b - a);

// BEGIN
describe('array sort', () => {
  test('sort numeric array asc/desc', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (arr) => {
          expect(sort(arr)).toBeSorted();
          expect(sortDesc(arr)).toBeSorted({ descending: true });

          expect(sort(arr).reverse()).toEqual(sortDesc(arr));
          expect(sortDesc(arr).reverse()).toEqual(sort(arr));
        })
    );
  });

  test('Invariance (the same elements and length)', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (arr) => {
          expect(sort(arr).length).toBe(arr.length);
          expect(sort(arr).every(elem => arr.indexOf(elem) !== -1)).toBeTruthy();
        })
    );
  });

  test('idempotency', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (arr) => {
          expect(arr.sort().sort()).toEqual(arr.sort());
        })
    );
  });

  test('modify origin array', () => {
    fc.assert(
      fc.property(
        fc.array(fc.nat(), { maxLength: 10 }),
        (arr) => {
          const sortedArr = arr.sort();

          expect(sortedArr).toEqual(arr);
          expect(sortedArr).toBe(arr);
        })
    );
  });
});

// END
