const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  test('should load home page', async () => {
    await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
    const titleText = await page.$eval('#title', (el) => el.textContent);

    expect(titleText).toBe('Welcome to a Simple blog!');
  });

  test('should go to the articles page with a list of articles', async () => {
    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });

    const titleText = await page.$eval('h3', (el) => el.textContent);
    const listElement = await page.$('#articles');

    expect(titleText).toBe('Articles');
    expect(listElement).not.toBeNull();
  });

  test('should successfully create an article', async () => {
    const title = 'Testing with puppeteerr';
    const category = "optio quo quis";
    const content = 'Puppeteer is a Node library which provides a high-level API to control Chromium or Chrome over the DevTools Protocol.';

    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });

    await page.click('a[href="/articles/new"]');
    await page.waitForSelector('form');

    const titleText1 = await page.$eval('h3', (el) => el.textContent);
    const formElement = await page.$('form');
    const url = await page.url();

    expect(url).toBe(`${appArticlesUrl}/new`);
    expect(titleText1).toBe('Create article');
    expect(formElement).not.toBeNull();

    await page.type('#name', title);
    await page.type('#category', category);
    await page.type('#content', content);
    await page.$eval('form', form => form.submit());

    await page.waitForSelector('#articles');
    const titleText2 = await page.$eval('h3', (el) => el.textContent);
    const listElement = await page.$('#articles');

    const isArticleInList = await page.$eval('#articles', (el, title, category) => {
      const rows = [...el.querySelectorAll('tr')];
      return rows.some(row => {
        const tds = [...row.querySelectorAll('td')].map(td => td.textContent);
        return tds[1] === title && tds[2] === category;
      });
    }, title, category);

    expect(titleText2).toBe('Articles');
    expect(listElement).not.toBeNull();
    expect(isArticleInList).toBe(true);
  });

  test('should successfully edit an article', async () => {
    const getFirstArticle = async () => {
      return await page.evaluate(() => {
        const rows = document.querySelectorAll('#articles tr');
        const tds = rows[1].querySelectorAll('td');
        return { id: tds[0].textContent, title: tds[1].textContent };
      });
    };

    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });

    const article = await getFirstArticle();

    await page.click(`a[href="/articles/${article.id}/edit"]`);
    await page.waitForSelector('#edit-form');
    const url = await page.url();

    expect(url).toBe(`${appArticlesUrl}/${article.id}/edit`);

    await page.evaluate(() => {
      document.querySelector('#name').value = '';
    });
    await page.type('#name', 'New title');
    await page.$eval('form', form => form.submit());

    await page.waitForSelector('#articles');

    const updatedArticle = await getFirstArticle();
    expect(updatedArticle.id).toBe(article.id);
    expect(updatedArticle.title).toBe('New title');
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
