test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();
  // BEGIN
  expect(result).toStrictEqual({ k: 'v', a: 'a', b: 'b' });
});

test('the same object', () => {
  const src = { k: 'v' };
  const target = { a: 'a' };

  const result = Object.assign(target, src);

  expect(result).toBe(target);
  expect(result).toStrictEqual(target);
});

test('should overwrite properties with the same keys', () => {
  const target = { a: 'a' };
  const src1 = { a: 'b' };
  const src2 = { a: 'c' };

  const result = Object.assign(target, src1, src2);

  expect(result).toStrictEqual({ a: 'c' });
});

test('throw an error if a property is non-writable', () => {
  const target = Object.create({}, {
    a: {
      value: 1,
      writable: false
    }});
  const src = { b: 'b', a: 'a' };

  expect(() => Object.assign(target, src)).toThrowError();
  // END
});