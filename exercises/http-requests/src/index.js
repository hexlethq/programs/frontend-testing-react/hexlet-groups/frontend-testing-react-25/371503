const axios = require('axios');

// BEGIN
const get = async (url) => {
  const response = await axios.get(url);
  return response.data;
};

const post = async (url, data) => {
  const response = await axios.post(url, data);
  return response.data;
}
// END

module.exports = { get, post };
