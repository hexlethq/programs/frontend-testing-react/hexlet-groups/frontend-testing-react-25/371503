const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN

const baseUrl = 'https://example.com';
const path = '/users';
const url = `${baseUrl}${path}`;

const errorCodes = [400, 404, 500];

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.cleanAll();
  nock.enableNetConnect();
});

describe('test get() anb post() functions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  test('get(), should return data from url', async () => {
    const defaultResult = {
      firstname: 'Fedor',
      lastname: 'Sumkin',
      age: 33
    };
    nock(baseUrl).get(path).reply(200, defaultResult);

    const res = await get(url);
    expect(res).toEqual(defaultResult);
    expect(nock.isDone()).toBeTruthy();
  });

  test('post(), should complete the reauest successfully', async () => {
    const data = {
      firstname: 'Fedor',
      lastname: 'Sumkin',
      age: 33
    };
    const result = { ...data, id: 5 };
    nock(baseUrl).post(path).reply(200, { ...result });

    const res = await post(url, data);

    expect(res).toEqual(result);
    expect(nock.isDone()).toBeTruthy();
  });

  test.each(errorCodes)('get(), should throw an error', async (code) => {
    nock(baseUrl).get('/path').reply(code);

    await expect(async () => get(`${baseUrl}/path`)).rejects.toThrow();
    expect(nock.isDone()).toBeTruthy();
  });

  test.each(errorCodes)('post(), should throw an error', async (code) => {
    const data = {
      firstname: 'Fedor',
      lastname: 'Sumkin'
    };
    nock(baseUrl).post(path).reply(code);

    await expect(post(url, data)).rejects.toThrow();
    expect(nock.isDone()).toBeTruthy();
  });
});
// END
