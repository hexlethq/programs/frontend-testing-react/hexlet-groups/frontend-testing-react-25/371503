const fs = require('fs');

// BEGIN
const getFileData = (path) => {
  const data = fs.readFileSync(path, 'utf8');
  return JSON.parse(data);
};

const writeFileData = (path, data) => {
  fs.writeFileSync(path, JSON.stringify(data), 'utf8');
};

const updateVersion = (str, type) => {
  const [major, minor, patch] = str.split('.').map(n => Number(n));
  let newVersion;

  switch (type) {
    case 'major':
      newVersion = [major + 1, 0, 0];
      break;
    case 'minor':
      newVersion = [major, minor + 1, 0];
      break;
    case 'patch':
      newVersion = [major, minor, patch + 1];
      break;
    default:
      throw Error('version type is invalid');
  };

  return newVersion.join('.');
};

const upVersion = (filePath, versionType = 'patch') => {
  const packageData = getFileData(filePath);
  const packageVersion = packageData.version;

  const newPackageData = {
    ...packageData,
    version: updateVersion(packageVersion, versionType)
  };

  writeFileData(filePath, newPackageData);
};
// END

module.exports = { upVersion };
