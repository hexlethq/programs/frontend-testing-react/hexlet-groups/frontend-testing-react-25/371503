const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const buildFixturePath = (file) => path.join(__dirname, '..', '__fixtures__', file);
const fixturePath = buildFixturePath('package.json');

const getFixtureData = (fixturePath) => {
  const data = fs.readFileSync(fixturePath, 'utf8');
  return JSON.parse(data);
}

const table = [
  {
    path: fixturePath,
    versionType: undefined,
    expected: { "version": "1.3.3" }
  },
  {
    path: fixturePath,
    versionType: 'major',
    expected: { "version": "2.0.0" }
  },
  {
    path: fixturePath,
    versionType: 'minor',
    expected: { "version": "1.4.0" }
  },
  {
    path: fixturePath,
    versionType: 'patch',
    expected: { "version": "1.3.3" }
  }
];

const invalidTable = [
  {
    path: 'invalidPath',
    versionType: 'patch'
  },
  {
    path: fixturePath,
    versionType: 'invalidType'
  }
];

describe('test upVersion function', () => {
  afterEach(() => {
    const defaultData = { "version": "1.3.2" };
    fs.writeFileSync(fixturePath, JSON.stringify(defaultData), 'utf8');
  });

  test.each(table)('update version: upVersion($path, $versionType)',
    ({ path, versionType, expected }) => {
      upVersion(path, versionType);
      expect(getFixtureData(fixturePath)).toEqual(expected);
    }
  );

  test.each(invalidTable)('throw an error with invalid arguments',
    ({ path, versionType }) => {
      expect(() => upVersion(path, versionType)).toThrowError();
    }
  );
});
// END
