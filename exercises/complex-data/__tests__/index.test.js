const faker = require('faker');

// BEGIN
describe('test faker.helpers.createTransaction()', () => {
  test('transaction should contain `amount`, `date`, `business`, `name`, `type`, `account`', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toMatchObject(expect.objectContaining({
      amount: expect.any(String),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.any(String)
    }));
  });

  test('transactions are not equal', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();

    expect(transaction1).not.toStrictEqual(transaction2);
  });
})

// END
